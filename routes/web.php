<?php

use App\User;
use App\Role;
use App\Image;
use App\Product;
use App\Category;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
//use App\Http\Controllers\Admin\AdminProductController;
//use App\Http\Controllers\Admin\AdminCategoryController;



/*Route::get('/prueba', function () {

    Gate::authorize('haveaccess','role.show');
    $user = User::find(1);



    return $user;



});*/
//mostrar resultados
Route::get('/resultados', function () {

   $image = App\Image::orderBy('id','Desc')->get();
   return  $image;
});




/*Route::get('/', function () {


return view('tienda.index');
});*/

Route::get('/home', 'HomeController@index')->name('home');


/*

Route::resource('/role', RoleController::class)->names('role');

Route::resource('/user', UserController::class, ['except'=>[
    'create','store']])->names('user');

*/



Auth::routes();


Route::get('/admin', function () {
    return view('plantilla.admin');
})->name('admin');


Route::get('cancelar/{ruta}', function($ruta) {
    return redirect()->route($ruta)->with('cancelar','Acción Cancelada!');
})->name('cancelar');



Route::get('/', function () {

    return view('welcome');


});
Route::get('/home', function () {

    $user=Auth::user();

        if($user->esAdmin()){

            //echo "eres admin";
            return view('home');

        }else{

            //echo 'eres estudiante';
            return view('home');
        }

    //return view('home');

});



Route::get('/role', function () {
    return view('role');
});

//Route::resource('empleados' , 'EmpleadosController');

Route::resource('student' , 'StudentController');

Route::resource('faculty' , 'FacultyController');

Route::resource('program' , 'ProgramController');

Route::resource('campus' , 'CampusController');
