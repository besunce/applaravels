<?php $__env->startSection('content'); ?>
</div>
<div class="container">
  <div class="row">
     <div class="col-md-12">
            </div>
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
         <div class="pull-right">
           <a class="btn btn-primary m-2" href="<?php echo e(url('campus/create')); ?>">agregar estudiante</a>
         </div>
        </div>
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="text-center" style="width: 10%;">id</th>
                <th class="text-center" style="width: 10%;"> nombre del campo </th>
              </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $campus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $campuse): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td class="text-center"> <?php echo e($campuse->id); ?></td>
                    <td class="text-center"> <?php echo e($campuse->name_camp); ?></td>
                    <?php if(@Auth::user()->esAdmin('administrador')): ?>
                    <td class="text-center">
                        <div class="btn-group">
                            <a class="btn btn-info btn-xs" href="<?php echo e(url('/campus/'.$campuse->id.'/edit' )); ?>">EDITAR</a>
                            <!--<a href="edit_product.php?id=4" class="btn btn-info btn-xs" title="" data-toggle="tooltip" data-original-title="Editar">
                                <span class="glyphicon glyphicon-edit">Editar</span>
                            </a>-->
                            
                            <form method="post" style="margin: 0px;"action="<?php echo e(url('/campus/'.$campuse->id)); ?>">
                                <?php echo e(csrf_field()); ?>

            
                                <?php echo e(method_field('DELETE')); ?>

                                <button class="btn btn-danger btn-xs" type="submit" onclick="return confirm('DELETE?');">BORRAR</button>
                            </form>
                            
                            <!--<a href="delete_product.php?id=4" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-original-title="Eliminar">
                                <span class="glyphicon glyphicon-trash">Eliminar</span>
                            </a>-->
                        </div>
                    </td>
                    <?php endif; ?>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
          </table>
          <a class="btn btn-primary m-2" href="<?php echo e(url('home')); ?>">regresar</a>
        </div>
      </div>
    </div>
  </div>
  <h2 class="tex-center">
<?php if(Session::has('Mensaje')): ?><?php echo e(Session::get('Mensaje')); ?>

<?php endif; ?>
</h2>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp2\htdocs\applaravels\resources\views/campus/index.blade.php ENDPATH**/ ?>