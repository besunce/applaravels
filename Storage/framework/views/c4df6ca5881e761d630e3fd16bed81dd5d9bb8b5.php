<?php $__env->startSection('content'); ?>
</div>
<div class="container">
  <div class="row">
     <div class="col-md-12">
            </div>
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
         <div class="pull-right">
         <?php if(@Auth::user()->esAdmin('administrador')): ?>
           <a class="btn btn-primary m-2" href="<?php echo e(url('program/create')); ?>">agregar programa</a>
           <?php endif; ?>
         </div>
        </div>
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="text-center" style="width: 50px;">id</th>
                <th>nombre del programa</th>
                <th>numero de la facultad </th>
              </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $program; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $programs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td class="text-center"> <?php echo e($programs->id); ?></td>
                    <td>                     <?php echo e($programs->name_progam); ?></td>
                    <td class="text-center"> <?php echo e($programs->faculty_id); ?></td>
                    <?php if(@Auth::user()->esAdmin('administrador')): ?>
                    <td class="text-center">
                        <div class="btn-group">
                            
                            <a class="btn btn-info btn-xs" href="<?php echo e(url('/program/'.$programs->id.'/edit' )); ?>">EDITAR</a>
                            <form method="post" style="margin: 0px;"action="<?php echo e(url('/program/'.$programs->id)); ?>">
                                <?php echo e(csrf_field()); ?>

            
                                <?php echo e(method_field('DELETE')); ?>

                                <button class="btn btn-danger btn-xs" type="submit" onclick="return confirm('DELETE?');">BORRAR</button>
                            </form>
                            
                        </div>
                    </td>
                    <?php endif; ?>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
          </table>
          <a class="btn btn-primary m-2" href="<?php echo e(url('home')); ?>">regresar</a>
        </div>
      </div>
    </div>
  </div>
  <h2 class="tex-center">
<?php if(Session::has('Mensaje')): ?><?php echo e(Session::get('Mensaje')); ?>

<?php endif; ?>
</h2>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp2\htdocs\applaravels\resources\views/program/index.blade.php ENDPATH**/ ?>