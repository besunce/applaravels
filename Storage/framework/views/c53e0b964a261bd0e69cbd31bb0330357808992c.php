<?php $__env->startSection('content'); ?>
</div>
<div class="container">
  <div class="row">
     <div class="col-md-12">
            </div>
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
         <div class="pull-right">
         <?php if(@Auth::user()->esAdmin('administrador')): ?>
                    <!--<h2>eres administrador</2><br>-->
                    <a class="btn btn-primary m-2" href="<?php echo e(url('student/create')); ?>">agregar estudiante</a>
         <?php endif; ?>
         </div>
        </div>
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="text-center" style="width: 50px;">Nombre</th>
                <th> Apellido</th>
                <th> CodEstudiante </th>
                <th class="text-center" style="width: 10%;"> Stock </th>
                <th class="text-center" style="width: 10%;"> Precio de compra </th>
                <th class="text-center" style="width: 10%;"> Precio de venta </th>
                <th class="text-center" style="width: 10%;"> Agregado </th>
                <th class="text-center" style="width: 100px;"> Acciones </th>
              </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $student; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $students): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td class="text-center"> <?php echo e($students->Nombre); ?></td>
                    <td>                     <?php echo e($students->Apellido); ?></td>
                    <td class="text-center"> <?php echo e($students->CodEstudiante); ?></td>
                    <td class="text-center"> <?php echo e($students->Direccion); ?></td>
                    <td class="text-center"> <?php echo e($students->Telefono); ?></td>
                    <td class="text-center"> <?php echo e($students->Telefono); ?></td>
                    <td class="text-center"> <?php echo e($students->Telefono); ?></td>
                    <td class="text-center">
                        <div class="btn-group">
                            <a class="btn btn-info btn-xs" href="<?php echo e(url('/student/'.$students->id.'/edit' )); ?>">EDITAR</a>
                            <!--<a href="edit_product.php?id=4" class="btn btn-info btn-xs" title="" data-toggle="tooltip" data-original-title="Editar">
                                <span class="glyphicon glyphicon-edit">Editar</span>
                            </a>-->
                            <?php if(@Auth::user()->esAdmin('administrador')): ?>
                            <form method="post" style="margin: 0px;"action="<?php echo e(url('/student/'.$students->id)); ?>">
                                <?php echo e(csrf_field()); ?>

            
                                <?php echo e(method_field('DELETE')); ?>

                                <button class="btn btn-danger btn-xs" type="submit" onclick="return confirm('DELETE?');">BORRAR</button>
                            </form>
                            <?php endif; ?>
                            <!--<a href="delete_product.php?id=4" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-original-title="Eliminar">
                                <span class="glyphicon glyphicon-trash">Eliminar</span>
                            </a>-->
                        </div>
                    </td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
          </table>
          <a class="btn btn-primary m-2" href="<?php echo e(url('home')); ?>">regresar</a>
        </div>
      </div>
    </div>
  </div>
  <h2 class="tex-center">
<?php if(Session::has('Mensaje')): ?><?php echo e(Session::get('Mensaje')); ?>

<?php endif; ?>
</h2>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp2\htdocs\applaravels\resources\views/student/index.blade.php ENDPATH**/ ?>