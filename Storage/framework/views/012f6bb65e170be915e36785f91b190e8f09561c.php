<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                <?php if(@Auth::user()->esAdmin('administrador')): ?>
                    <!--<h2>eres administrador</2><br>-->
                    <a href="<?php echo e(url('/student')); ?>">Estudiante</a>
                    <a href="<?php echo e(url('/faculty')); ?>">facultades</a>
                    <a href="<?php echo e(url('/program')); ?>">programas</a>
                    <a href="<?php echo e(url('/campus')); ?>">campus</a>
                <?php else: ?>
                    <!--<h2>eres estudiante</2><br>-->
                    <a href="<?php echo e(url('/student')); ?>">Estudiante</a>
                    <a href="<?php echo e(url('/faculty')); ?>">facultades</a>
                    <a href="<?php echo e(url('/program')); ?>">programas</a>
                    <a href="<?php echo e(url('/campus')); ?>">campus</a>

                <?php endif; ?>

                    </div>

                <div class="card-body">y
                    <?php if(session('status')): ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp2\htdocs\applaravels\resources\views/home.blade.php ENDPATH**/ ?>
