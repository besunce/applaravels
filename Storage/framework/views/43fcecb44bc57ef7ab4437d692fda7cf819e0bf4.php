<h1>crear empleados</h1>

<style>

.floating-label-form-group {
    position: relative;
    border-bottom: 1px solid #e9ecef;
}

.form-control {
    display: block;
    width: 50%;
    height: calc(1.5em + 1rem);
    padding: 0.375rem 0.75rem;
        padding-right: 0.75rem;
        padding-left: 0.75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 0.125rem solid #ced4da;
    border-radius: 0.5rem;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
}
button, input {
    overflow: visible;
}
.container, .container-fluid{
    width: 100%;
    padding-right: 1.5rem;
    padding-left: 1.5rem;
    margin-right: auto;
    margin-left: auto;
}
.btn-xl {
    padding: 12px 10px;
    font-size: 0.85rem;
    font-weight: 700;
    text-transform: uppercase;
    border: none;
    border-radius: 10rem;
}
.btn {
    font-family: "Merriweather Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
}
.btn-light {    
    background-color: #3b83cc;
    border-color: #f8f9fa;
}
.btn-l {
    background-color: #3ada60ba;
    border-color: #f8f9fa;
    text-decoration: none;
}
.btn {
    display: inline-block;
    font-weight: 400;
    color: #212529;
    text-align: center;
    vertical-align: middle;
    padding: 0.375rem 0.75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: 0.25rem;
    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
}
</style>

<link rel="stylesheet" href="bootstrap.css">

<form action="<?php echo e(url('/empleados')); ?>" method="post" enctype="multipart/form-data" >
<?php echo e(csrf_field()); ?>


<?php echo $__env->make('empleados.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>



</form><?php /**PATH C:\xampp2\htdocs\crud\resources\views/empleados/create.blade.php ENDPATH**/ ?>