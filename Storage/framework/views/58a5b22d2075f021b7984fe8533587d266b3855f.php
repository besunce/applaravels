<?php $__env->startSection('content'); ?>
<div class="container">
 <div class="row">
   <div class="col-md-12">  </div>
  <div class="col-md-6">
     <div class="panel panel-default">
       <div class="panel-heading">
        <strong>
          <span class="glyphicon glyphicon-th"></span>
          agragar estudiante</strong>
       </div>
       <div class="panel-body">
            <div class="form-group">
                  
                  <label for="Nombre" class="control-label"><?php echo e('Nombre'); ?></label>
                  <input class="form-control" type="text" name="Nombre" id="Nombre" value="">

            </div>
            <div class="form-group">
                    <label for="Apellido" class="control-label" ><?php echo e('Apellido'); ?></label>
                    <input class="form-control" type="text" name="Apellido" id="Apellido" value="">
            </div>
            <div class="form-group">
                    <label for="CodigoEstudiante" class="control-label"><?php echo e('CodigoEstudiante'); ?></label>
                    <input class="form-control" type="text" name="CodigoEstudiante" id="CodigoEstudiante" value="">
            </div>
            <div class="form-group">
            <label for="Direccion"><?php echo e('Direccion'); ?></label>
            <input class="form-control" type="text" name="Direccion" id="Direccion" value="">
            </div>
            <div class="form-group">
            <label for="Telefono"><?php echo e('Telefono'); ?></label>
            <input class="form-control" type="text" name="Telefono" id="Telefono" value="">
            </div>
            <div class="form-group">
            <label for="Recidenca"><?php echo e('Recidenca'); ?></label>
            <input class="form-control" type="text" name="Recidenca" id="Recidenca" value="">
            <div>
            <div class="form-group">
            <label for="Origen"><?php echo e('Origen'); ?></label>
            <input class="form-control" type="text" name="Origen" id="Origen" value="">
            </div>
            
            <div class="form-group">
            <label for="Nacionalidad"><?php echo e('Nacionalidad'); ?></label>
            <input class="form-control" type="text" name="Nacionalidad" id="Nacionalidad" value="">
            </div>
            <div class="form-group">
            <label for="programa"><?php echo e('programa'); ?></label>
            <input class="form-control" type="text" name="programa" id="programa" value="">
            </div>

            <!--<div class="form-group">
              <label for="level">Rol de usuario</label>
                <select class="form-control" name="level">
                                   <option selected="selected" value="1">Admin</option>
                                   <option value="2">Special</option>
                                   <option value="3">User</option>
                                </select>
            </div>-->

            <div class="form-group clearfix">
                    <input class="btn btn-info" type="submit" value="AGREGAR">
                    
                    <!--<button type="submit" name="update" class="btn btn-info">Actualizar</button>-->
                    <a class="btn btn-danger pull-right" href="<?php echo e(url('estudiantes')); ?>">REGRESAR</a>
            </div>
        </form>
       </div>
     </div>
  </div>
 </div>
</div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp2\htdocs\crud\resources\views/estudiantes/form.blade.php ENDPATH**/ ?>