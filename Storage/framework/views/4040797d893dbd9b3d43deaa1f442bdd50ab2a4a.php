
<style>

.container, .container-fluid{
    width: 100%;
    padding-right: 1.5rem;
    padding-left: 1.5rem;
    margin-right: auto;
    margin-left: auto;
}

.editar {
    background-color: #ffe84c;
    border-color: #f8f9fa;
    text-decoration: none;
}
.borrar {
    background-color: #e51232;
    border-color: #f8f9fa;
    text-decoration: none;
}
.agregar{
    background-color: #1ebd6d;
    border-color: #f8f9fa;
    text-decoration: none;
}
.btn {
    display: inline-block;
    font-weight: 400;
    color: #ffffff;
    text-align: center;
    vertical-align: middle;
    padding: 0.375rem 0.75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: 0.25rem;
    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
}
.x {
    padding: 12px 10px;
    font-size: 0.85rem;
    font-weight: 700;
    text-transform: uppercase;
    border: none;
    border-radius: 10rem;
}
tr{

    height:90px;

}
.table-borde {
    border: 1px solid #ddd;
    width: 100%;
}
.tex-center{
    text-align:center;
}
.titu{
    border: 1px solid #ddd;
    background: lightskyblue;
}
.msj{

    
}



</style>






<div class="container">

<a class="btn agregar x" href="<?php echo e(url('empleados/create')); ?>">agregar empleado</a>

<table class="table table-borde ">
    <thead class="thead-linght">
        <tr class="titu">
            <th style="width: 3%;">#</th>
            <th style="width: 7%;">Foto</th>
            <th style="width: 10%;">Nombre</th>
            <th style="width: 10%;">PrimerApellido</th>
            <th style="width: 10%;">SegundoApellido</th>
            <th style="width: 25%;">Correo</th>
            <th style="width: 10%;">acciones</th>
        </tr>
    
    </thead>
    <tbody>
    <?php $__currentLoopData = $empleados; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $empleado): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td class="tex-center"><?php echo e($loop->iteration); ?></td>
            <td>

            <img src="<?php echo e(asset('storage').'/'. $empleado->Foto); ?>" alt="" width="100">
            
            </td>
            <td class="tex-center"><?php echo e($empleado->Nombre); ?></td>
            <td class="tex-center"><?php echo e($empleado->PrimerApellido); ?></td>
            <td class="tex-center"><?php echo e($empleado->SegundoApellido); ?></td>
            <td class="tex-center"><?php echo e($empleado->Correo); ?></td>
            <td>

            <a class="btn editar x" href="<?php echo e(url('/empleados/'.$empleado->id.'/edit' )); ?>">
            EDITAR
            </a>
            
            <form method="post" action="<?php echo e(url('/empleados/'.$empleado->id)); ?>">
            <?php echo e(csrf_field()); ?>

            
            <?php echo e(method_field('DELETE')); ?>

            <button class="btn borrar x" type="submit" onclick="return confirm('DELETE?');">BORRAR</button>
            </form>
            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>



</table>
<h1 class="tex-center">
<?php if(Session::has('Mensaje')): ?><?php echo e(Session::get('Mensaje')); ?>

<?php endif; ?>
</h1>

</div><?php /**PATH C:\xampp2\htdocs\crud\resources\views/empleados/index.blade.php ENDPATH**/ ?>