

@extends('layouts.app')

@section('content')
<div class="container">
 <div class="row">
   <div class="col-md-12">  </div>
  <div class="col-md-6">
     <div class="panel panel-default">
       <div class="panel-heading">
        <strong>
          <span class="glyphicon glyphicon-th"></span>
          editar estudiantes</strong>
       </div>
       <div class="panel-body">
          <!--<form method="post" action="edit_user.php?id=1" class="clearfix">-->
        <form action="{{ url('/faculty/'.$faculties->id) }}" method="post" class="clearfix" >
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="form-group">
                  
                  <label for="Nombre" class="control-label">{{'Nombre'}}</label>
                  <input class="form-control" type="text" name="Nombre" id="Nombre" value="{{ $student->Nombre }}">

            </div>
            <div class="form-group">
                    <label for="Apellido" class="control-label" >{{'Apellido'}}</label>
                    <input class="form-control" type="text" name="Apellido" id="Apellido" value="{{ $student->Apellido }}">
            </div>
            <div class="form-group">
                    <label for="CodEstudiante" class="control-label">{{'CodEstudiante'}}</label>
                    <input class="form-control" type="text" name="CodEstudiante" id="CodEstudiante" value="{{ $student->CodEstudiante}}">
            </div>
            <div class="form-group">
            <label for="Direccion">{{'Direccion'}}</label>
            <input class="form-control" type="text" name="Direccion" id="Direccion" value="{{ $student->Direccion }}">
            </div>
            <div class="form-group">
            <label for="Telefono">{{'Telefono'}}</label>
            <input class="form-control" type="text" name="Telefono" id="Telefono" value="{{ $student->Telefono }}">
            </div>
            <div class="form-group">
            <label for="Recidenca">{{'Recidenca'}}</label>
            <input class="form-control" type="text" name="Recidenca" id="Recidenca" value="{{ $student->Recidenca }}">
            <div>
            <div class="form-group">
            <label for="Origen">{{'Origen'}}</label>
            <input class="form-control" type="text" name="Origen" id="Origen" value="{{ $student->Origen }}">
            </div>
            
            <div class="form-group">
            <label for="Nacionalidad">{{'Nacionalidad'}}</label>
            <input class="form-control" type="text" name="Nacionalidad" id="Nacionalidad" value="{{ $student->Nacionalidad }}">
            </div>
            <div class="form-group">
            <label for="programa">{{'programa'}}</label>
            <input class="form-control" type="text" name="programa" id="programa" value="{{ $student->programa }}">
            </div>

            <!--<div class="form-group">
              <label for="level">Rol de usuario</label>
                <select class="form-control" name="level">
                                   <option selected="selected" value="1">Admin</option>
                                   <option value="2">Special</option>
                                   <option value="3">User</option>
                                </select>
            </div>-->

            <div class="form-group clearfix">
                    <input class="btn btn-info" type="submit" value="MODIFICAR">
                    
                    <!--<button type="submit" name="update" class="btn btn-info">Actualizar</button>-->
                    <a class="btn btn-danger pull-right" href="{{ url('faculty')}}">REGRESAR</a>
            </div>
        </form>
       </div>
     </div>
  </div>
  <!-- Change password form -->
  <!--<div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        <strong>
          <span class="glyphicon glyphicon-th"></span>
          Cambiar Admin Users contraseña
        </strong>
      </div>
      <div class="panel-body">
        <form action="edit_user.php?id=1" method="post" class="clearfix">
          <div class="form-group">
                <label for="password" class="control-label">Contraseña</label>
                <input type="password" class="form-control" name="password" placeholder="Ingresa la nueva contraseña" required="">
          </div>
          <div class="form-group clearfix">
                  <button type="submit" name="update-pass" class="btn btn-danger pull-right">Cambiar</button>
          </div>
        </form>
      </div>
    </div>
  </div>-->

 </div>
</div>
@endsection