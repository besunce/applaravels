
@extends('layouts.app')


@section('content')
</div>
<div class="container">
  <div class="row">
     <div class="col-md-12">
            </div>
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
         <div class="pull-right">
           <a class="btn btn-primary m-2" href="{{ url('faculty/create')}}">agregar facultades</a>
         </div>
        </div>
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="text-center" style="width: 50px;">id</th>
                <th>Nombre facultad</th>
                <th> CodEstudiante </th>
                <th class="text-center" style="width: 10%;"> Nombre_campuso </th>
              </tr>
            </thead>
            <tbody>
            @foreach($faculty as $faculties)
                <tr>
                    <td class="text-center"> {{ $faculties->id}}</td>
                    <td>                     {{ $faculties->name_fac}}</td>
                    <td class="text-center"> {{ $faculties->campus_id}}</td>
                    <td class="text-center">
                        <div class="btn-group">
                            <a class="btn btn-info btn-xs" href="{{ url('/faculty/'.$faculties->id.'/edit' ) }}">EDITAR</a>
                            @if(@Auth::user()->esAdmin('administrador'))
                            <form method="post" style="margin: 0px;"action="{{ url('/faculty/'.$faculties->id) }}">
                                {{csrf_field() }}
            
                                {{ method_field('DELETE')}}
                                <button class="btn btn-danger btn-xs" type="submit" onclick="return confirm('DELETE?');">BORRAR</button>
                            </form>
                            @endif
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
          <a class="btn btn-primary m-2" href="{{ url('home')}}">regresar</a>
        </div>
      </div>
    </div>
  </div>
  <h2 class="tex-center">
@if(Session::has('Mensaje')){{


    Session::get('Mensaje')

}}
@endif
</h2>
</div>

@endsection