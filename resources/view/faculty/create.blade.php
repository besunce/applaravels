@if(@Auth::user()->esAdmin('administrador'))
<form action="{{ url('/faculty') }}" method="post" >
{{ csrf_field() }}

@include('faculty.form')

</form>
@else

sin acceso
<a class="btn btn-primary m-2" href="{{ url('home')}}">regresar</a>

@endif