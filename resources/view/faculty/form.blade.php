
@extends('layouts.app')

@section('content')
<div class="container">
 <div class="row">
   <div class="col-md-12">  </div>
  <div class="col-md-6">
     <div class="panel panel-default">
       <div class="panel-heading">
        <strong>
          <span class="glyphicon glyphicon-th"></span>
          agregar facultades</strong>
       </div>
       <div class="panel-body">
            <div class="form-group">
                  
                  <label for="id" class="control-label">{{'id'}}</label>
                  <input class="form-control" type="text" name="id" id="id" value="">

            </div>
            <div class="form-group">
                    <label for="name_fac" class="control-label" >{{'nombre facultad'}}</label>
                    <input class="form-control" type="text" name="name_fac" id="name_fac" value="">
            </div>
            
            <div class="form-group">
              <label for="campus_id">{{'edificio'}}</label>
                <select class="form-control" name="campus_id">
                                   <option selected="selected" value="1">edificio 1</option>
                                   <option value="2">edificio 2</option>
                                </select>
            </div>

            <div class="form-group clearfix">
                    <input class="btn btn-info" type="submit" value="AGREGAR">
                    
                    <!--<button type="submit" name="update" class="btn btn-info">Actualizar</button>-->
                    <a class="btn btn-danger pull-right" href="{{ url('faculty')}}">REGRESAR</a>
            </div>
        </form>
       </div>
     </div>
  </div>
 </div>
</div>
@endsection

