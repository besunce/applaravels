@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                @if(@Auth::user()->esAdmin('administrador'))
                    <!--<h2>eres administrador</2><br>-->
                    <a href="{{ url('/student') }}">Estudiante</a>
                    <a href="{{ url('/faculty') }}">facultades</a>
                    <a href="{{ url('/program') }}">programas</a>
                    <a href="{{ url('/campus') }}">campus</a>
                @else
                    <!--<h2>eres estudiante</2><br>-->
                    <a href="{{ url('/student') }}">Estudiante</a>
                    <a href="{{ url('/faculty') }}">facultades</a>
                    <a href="{{ url('/program') }}">programas</a>
                    <a href="{{ url('/campus') }}">campus</a>
                
                @endif
                    
                    </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
