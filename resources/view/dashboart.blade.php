@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">estudiantes</div>
                <strong><a href="{{ url('/student') }}">Estudiante</a></strong>
                <strong><a href="{{ url('/faculty') }}">facultades</a></strong>
                <strong><a href="{{ url('/program') }}">programas</a></strong>
                <strong><a href="{{ url('/campus') }}">campus</a></strong>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection