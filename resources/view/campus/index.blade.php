
@extends('layouts.app')


@section('content')
</div>
<div class="container">
  <div class="row">
     <div class="col-md-12">
            </div>
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
         <div class="pull-right">
           <a class="btn btn-primary m-2" href="{{ url('campus/create')}}">agregar estudiante</a>
         </div>
        </div>
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="text-center" style="width: 10%;">id</th>
                <th class="text-center" style="width: 10%;"> nombre del campo </th>
              </tr>
            </thead>
            <tbody>
            @foreach($campus as $campuse)
                <tr>
                    <td class="text-center"> {{ $campuse->id}}</td>
                    <td class="text-center"> {{ $campuse->name_camp}}</td>
                    @if(@Auth::user()->esAdmin('administrador'))
                    <td class="text-center">
                        <div class="btn-group">
                            <a class="btn btn-info btn-xs" href="{{ url('/campus/'.$campuse->id.'/edit' ) }}">EDITAR</a>
                            <!--<a href="edit_product.php?id=4" class="btn btn-info btn-xs" title="" data-toggle="tooltip" data-original-title="Editar">
                                <span class="glyphicon glyphicon-edit">Editar</span>
                            </a>-->
                            
                            <form method="post" style="margin: 0px;"action="{{ url('/campus/'.$campuse->id) }}">
                                {{csrf_field() }}
            
                                {{ method_field('DELETE')}}
                                <button class="btn btn-danger btn-xs" type="submit" onclick="return confirm('DELETE?');">BORRAR</button>
                            </form>
                            
                            <!--<a href="delete_product.php?id=4" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-original-title="Eliminar">
                                <span class="glyphicon glyphicon-trash">Eliminar</span>
                            </a>-->
                        </div>
                    </td>
                    @endif
                </tr>
                @endforeach
            </tbody>
          </table>
          <a class="btn btn-primary m-2" href="{{ url('home')}}">regresar</a>
        </div>
      </div>
    </div>
  </div>
  <h2 class="tex-center">
@if(Session::has('Mensaje')){{


    Session::get('Mensaje')

}}
@endif
</h2>
</div>

@endsection