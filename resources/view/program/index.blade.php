
@extends('layouts.app')


@section('content')
</div>
<div class="container">
  <div class="row">
     <div class="col-md-12">
            </div>
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
         <div class="pull-right">
         @if(@Auth::user()->esAdmin('administrador'))
           <a class="btn btn-primary m-2" href="{{ url('program/create')}}">agregar programa</a>
           @endif
         </div>
        </div>
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="text-center" style="width: 50px;">id</th>
                <th>nombre del programa</th>
                <th>numero de la facultad </th>
              </tr>
            </thead>
            <tbody>
            @foreach($program as $programs)
                <tr>
                    <td class="text-center"> {{ $programs->id}}</td>
                    <td>                     {{ $programs->name_progam}}</td>
                    <td class="text-center"> {{ $programs->faculty_id}}</td>
                    @if(@Auth::user()->esAdmin('administrador'))
                    <td class="text-center">
                        <div class="btn-group">
                            
                            <a class="btn btn-info btn-xs" href="{{ url('/program/'.$programs->id.'/edit' ) }}">EDITAR</a>
                            <form method="post" style="margin: 0px;"action="{{ url('/program/'.$programs->id) }}">
                                {{csrf_field() }}
            
                                {{ method_field('DELETE')}}
                                <button class="btn btn-danger btn-xs" type="submit" onclick="return confirm('DELETE?');">BORRAR</button>
                            </form>
                            
                        </div>
                    </td>
                    @endif
                </tr>
                @endforeach
            </tbody>
          </table>
          <a class="btn btn-primary m-2" href="{{ url('home')}}">regresar</a>
        </div>
      </div>
    </div>
  </div>
  <h2 class="tex-center">
@if(Session::has('Mensaje')){{


    Session::get('Mensaje')

}}
@endif
</h2>
</div>

@endsection
