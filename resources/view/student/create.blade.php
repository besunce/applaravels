@if(@Auth::user()->esAdmin('administrador'))
<form action="{{ url('/student') }}" method="post" >
{{ csrf_field() }}

@include('student.form')



</form>
@else

sin acceso
<a class="btn btn-primary m-2" href="{{ url('home')}}">regresar</a>

@endif