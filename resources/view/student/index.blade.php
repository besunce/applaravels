
@extends('layouts.app')


@section('content')
</div>
<div class="container">
  <div class="row">
     <div class="col-md-12">
            </div>
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
         <div class="pull-right">
         @if(@Auth::user()->esAdmin('administrador'))
                    <!--<h2>eres administrador</2><br>-->
                    <a class="btn btn-primary m-2" href="{{ url('student/create')}}">agregar estudiante</a>
         @endif
         </div>
        </div>
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="text-center" style="width: 50px;">Nombre</th>
                <th> Apellido</th>
                <th> CodEstudiante </th>
                <th class="text-center" style="width: 10%;"> Stock </th>
                <th class="text-center" style="width: 10%;"> Precio de compra </th>
                <th class="text-center" style="width: 10%;"> Precio de venta </th>
                <th class="text-center" style="width: 10%;"> Agregado </th>
                <th class="text-center" style="width: 100px;"> Acciones </th>
              </tr>
            </thead>
            <tbody>
            @foreach($student as $students)
                <tr>
                    <td class="text-center"> {{ $students->Nombre}}</td>
                    <td>                     {{ $students->Apellido}}</td>
                    <td class="text-center"> {{ $students->CodEstudiante}}</td>
                    <td class="text-center"> {{ $students->Direccion}}</td>
                    <td class="text-center"> {{ $students->Telefono}}</td>
                    <td class="text-center"> {{ $students->Telefono}}</td>
                    <td class="text-center"> {{ $students->Telefono}}</td>
                    <td class="text-center">
                        <div class="btn-group">
                            <a class="btn btn-info btn-xs" href="{{ url('/student/'.$students->id.'/edit' ) }}">EDITAR</a>
                            <!--<a href="edit_product.php?id=4" class="btn btn-info btn-xs" title="" data-toggle="tooltip" data-original-title="Editar">
                                <span class="glyphicon glyphicon-edit">Editar</span>
                            </a>-->
                            @if(@Auth::user()->esAdmin('administrador'))
                            <form method="post" style="margin: 0px;"action="{{ url('/student/'.$students->id) }}">
                                {{csrf_field() }}
            
                                {{ method_field('DELETE')}}
                                <button class="btn btn-danger btn-xs" type="submit" onclick="return confirm('DELETE?');">BORRAR</button>
                            </form>
                            @endif
                            <!--<a href="delete_product.php?id=4" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-original-title="Eliminar">
                                <span class="glyphicon glyphicon-trash">Eliminar</span>
                            </a>-->
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
          <a class="btn btn-primary m-2" href="{{ url('home')}}">regresar</a>
        </div>
      </div>
    </div>
  </div>
  <h2 class="tex-center">
@if(Session::has('Mensaje')){{


    Session::get('Mensaje')

}}
@endif
</h2>
</div>

@endsection